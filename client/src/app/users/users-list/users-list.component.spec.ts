import { UsersRoutingModule } from './../users.routing';
import { SharedModule } from './../../shared/shared.module';
import { NotificationService } from './../../core/services/notification.service';
import { UsersService } from './../../core/services/users.service';
import { AuthService } from './../../core/services/auth.service';
import { UsersListComponent } from './users-list.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserInfoComponent } from '../user-info/user-info.component';
import { of } from 'rxjs';

describe('UsersListComponent', () => {
  let component: UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;
  const authService = {
    get loggedUserData$() {
      return of('test');
    },
  };

  const usersService = {
    findUsers() { }
  };

  const notificationService = {};

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SharedModule, UsersRoutingModule],
      declarations: [UsersListComponent, UserInfoComponent],
      providers: [AuthService, UsersService, NotificationService]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .compileComponents();

    fixture = TestBed.createComponent(UsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('onScroll() should', () => {
    it('call usersService.findUsers with correct parameters', () => {
      const findUsersSpy = spyOn(usersService, 'findUsers').and.returnValue(of([]));
      const keyword = component.keyword;
      const skip = component.skip;
      const take = component.take;

      component.onScroll();

      expect(findUsersSpy).toHaveBeenCalledWith(keyword, skip, take);
      expect(findUsersSpy).toHaveBeenCalledTimes(1);

    });

    it('add new users from the usersService.findUsers() returned value', () => {
      spyOn(usersService, 'findUsers').and.returnValue(of(['u1', 'u2', 'u3']));

      component.onScroll();

      expect(component.users).toEqual(['u1', 'u2', 'u3']);
    });

    it('increments skip with the amount of take', () => {
      component.take = 5;
      component.skip = 0;
      spyOn(usersService, 'findUsers').and.returnValue(of([]));

      component.onScroll();

      expect(component.skip).toEqual(5);
    });
  });

  describe('search() should', () => {
    it('set the keyword field with the accepted input parameter', () => {
      spyOn(usersService, 'findUsers').and.returnValue(of([]));
      const keyword = 'word';

      component.search(keyword);

      expect(component.keyword).toEqual(keyword);
    });

    it('set the skip field to 0', () => {
      spyOn(usersService, 'findUsers').and.returnValue(of([]));
      const keyword = 'word';

      component.search(keyword);

      expect(component.skip).toEqual(0);
    });

    it('call usersService.findUsers with correct parameters', () => {
      const findUsersSpy = spyOn(usersService, 'findUsers').and.returnValue(of([]));
      const keyword = 'test-word';
      const skip = component.skip;
      const take = component.take;

      component.search(keyword);

      expect(findUsersSpy).toHaveBeenCalledTimes(1);
      expect(findUsersSpy).toHaveBeenCalledWith(keyword, skip, take);
    });

    it('sets the users field with the result from the usersService.findUsers() method', () => {
      spyOn(usersService, 'findUsers').and.returnValue(of(['u1', 'u2', 'u3']));
      const keyword = 'test-word';

      component.search(keyword);

      expect(component.users).toEqual(['u1', 'u2', 'u3']);
    });
  });
});
