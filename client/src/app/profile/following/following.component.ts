import { UserDTO } from './../../models/user/user.dto';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss']
})
export class FollowingComponent {
  @Input() following: UserDTO[];
}
