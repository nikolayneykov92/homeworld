import { UserLoginDTO } from '../models/user/user-login.dto';
import { AuthenticationService } from './authentication.service';
import { Controller, Post, Body, ValidationPipe, UseFilters } from '@nestjs/common';
import { HomeWorldApplicationExceptionFilter } from '../filters/home-world-application-exception.filter';
import { ApiUseTags } from '@nestjs/swagger';

@Controller('api/session')
@UseFilters(HomeWorldApplicationExceptionFilter)
@ApiUseTags('authentication')
export class AuthenticationController {
  constructor(
    private readonly authenticationService: AuthenticationService,
  ) { }

  @Post()
  async login(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserLoginDTO,
  ): Promise<{ authToken: string }> {
    return await this.authenticationService.login(user);
  }
}
