import { UsersService } from './users.service';
import { async, TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { of } from 'rxjs';

describe('UsersService', () => {
  const FAKE_API_URL = 'http://localhost:3000/api';
  const fakeToken = 'authToken';
  let httpClient;
  let storageService;
  let authService;
  let usersService: UsersService;


  beforeEach(async () => {
    httpClient = {
      get() { },
      post() { },
      delete() { }
    };

    storageService = {
      getItem() { return fakeToken; },
      setItem() { },
      removeItem() { }
    };

    authService = {
      isTokenExpired() { },
      decodeToken() { }
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [AuthService, StorageService, UsersService]
    })
      .overrideProvider(HttpClient, { useValue: httpClient })
      .overrideProvider(StorageService, { useValue: storageService })
      .overrideProvider(AuthService, { useValue: authService });

    usersService = TestBed.get(UsersService);
  });

  it('should be created', () => {
    expect(usersService).toBeTruthy();
  });

  describe(('findUsers() should'), () => {
    it('call http.get() method with correct parameters', () => {
      const fakeKeyword = 'keyword';
      const fakeTake = 10;
      const fakeSkip = 20;
      const fakeUrl = `${FAKE_API_URL}/users?keyword=${fakeKeyword}&take=${fakeTake}&skip=${fakeSkip}`;
      const getSpy = spyOn(httpClient, 'get');

      usersService.findUsers(fakeKeyword, fakeSkip, fakeTake);

      expect(getSpy).toHaveBeenCalledTimes(1);
      expect(getSpy).toHaveBeenCalledWith(fakeUrl);
    });
  });
});
