import { IsString, Length, IsPositive, IsNumber, IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { UserDTO } from '../user/user.dto';

export class CommentDTO {
  @IsNumber()
  @IsPositive()
  @ApiModelProperty()
  id: number;

  @IsString()
  @Length(0, 200)
  @ApiModelProperty()
  content: string;

  @IsBoolean()
  liked: boolean;

  @Exclude()
  isDeleted: boolean;

  @ApiModelProperty()
  creator: UserDTO;
}
