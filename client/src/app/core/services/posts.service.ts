import { PostDTO } from './../../models/post/post.dto';
import { Injectable } from '@angular/core';
import { API_URL } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostUpdateDTO } from '../../models/post/post-update.dto';
import { PostFullDTO } from '../../models/post/post-full.dto';
import { PostLikeDTO } from '../../models/post/post-like.dto';
import { PostLocationDTO } from '../../models/post/post-location.dto';

@Injectable()
export class PostsService {
  constructor(
    private readonly http: HttpClient,
  ) { }

  findPosts(skip: number, take: number): Observable<PostDTO[]> {
    return this.http.get<PostDTO[]>(`${API_URL}/posts?skip=${skip}&take=${take}`);
  }

  findPublicPosts(skip: number, take: number): Observable<PostDTO[]> {
    return this.http.get<PostDTO[]>(`${API_URL}/posts/public?skip=${skip}&take=${take}`);
  }

  findFollowersPosts(id: number, skip: number, take: number): Observable<PostDTO[]> {
    return this.http.get<PostDTO[]>(`${API_URL}/posts/followers?skip=${skip}&take=${take}&id=${id}`);
  }

  findFollowingPosts(id: number, skip: number, take: number): Observable<PostDTO[]> {
    return this.http.get<PostDTO[]>(`${API_URL}/posts/following?skip=${skip}&take=${take}&id=${id}`);
  }

  findPostById(id: number): Observable<PostFullDTO> {
    return this.http.get<PostFullDTO>(`${API_URL}/posts/${id}`);
  }

  createPost(post: FormData): Observable<PostDTO> {
    return this.http.post<PostDTO>(`${API_URL}/posts`, post);
  }

  editPost(id: number, updatedPost: PostUpdateDTO): Observable<PostFullDTO> {
    return this.http.put<PostFullDTO>(`${API_URL}/posts/${id}`, updatedPost);
  }

  deletePost(id: number): Observable<PostDTO> {
    return this.http.delete<PostDTO>(`${API_URL}/posts/${id}`);
  }

  likePost(postId: number, like: PostLikeDTO): Observable<PostFullDTO> {
    return this.http.post<PostFullDTO>(`${API_URL}/posts/${postId}/likes`, like);
  }

  getLocation(): Observable<PostLocationDTO> {
    return this.http.get<PostLocationDTO>(`https://ipapi.co/json/`);
  }
}
