import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AnonymousGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public canActivate(): boolean {
    if (this.authService.getLoggedUserData()) {
      this.router.navigate(['/home-world']);

      return false;
    }

    return true;
  }
}
