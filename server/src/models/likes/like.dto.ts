import { IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class LikeDTO {
  @IsBoolean()
  @ApiModelProperty()
  liked: boolean;
}
