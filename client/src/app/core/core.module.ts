import { AnonymousGuard } from './guards/anonymous.guard';
import { AuthGuard } from './guards/auth.guard';
import { CreatedPostsResolver } from './resolvers/created-posts.resolver';
import { LikedPostsResolver } from './resolvers/liked-posts.resolver';
import { SocketService } from './services/socket.service';
import { SharedModule } from './../shared/shared.module';
import { NotificationService } from './services/notification.service';
import { UsersResolver } from './resolvers/users.resolver';
import { ProfileResolver } from './resolvers/profile.resolver';
import { PostsResolver } from './resolvers/posts.resolver';
import { PostsService } from './services/posts.service';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { UsersService } from './services/users.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { ToastrModule } from 'ngx-toastr';
import { CommentsService } from './services/comments.service';
import { FolowersPostsResolver } from './resolvers/folllowers-posts.resolver';
import { FollowingPostsResolver } from './resolvers/following-posts.resolver';

@NgModule({
  providers: [
    PostsResolver,
    LikedPostsResolver,
    CreatedPostsResolver,
    FolowersPostsResolver,
    FollowingPostsResolver,
    UsersResolver,
    ProfileResolver,
    AuthService,
    StorageService,
    UsersService,
    PostsService,
    NotificationService,
    SocketService,
    CommentsService,
    AuthGuard,
    AnonymousGuard,
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    JwtModule.forRoot({ config: {} }),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      return parent;
    }
  }
}
