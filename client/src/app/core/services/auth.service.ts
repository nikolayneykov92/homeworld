import { SocketService } from './socket.service';
import { UserLoginDTO } from './../../models/user/user-login.dto';
import { API_URL } from '../../config/config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDTO } from '../../models/user/user.dto';

@Injectable()
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(this.getLoggedUserData());

  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly socketService: SocketService,
    private readonly jwtHelperService: JwtHelperService,
  ) { }

  get loggedUserData$(): Observable<UserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }

  login(user: UserLoginDTO): Observable<{ authToken: string }> {
    return this.http.post<{ authToken: string }>(API_URL + '/session', user)
      .pipe(
        tap(({ authToken }) => {
          this.storageService.setItem('authToken', authToken);
          this.storageService.setItem('email',  this.getLoggedUserData().email);
          this.storageService.setItem('username', this.getLoggedUserData().username);
          this.storageService.setItem('joined', true);
          this.socketService.join(this.storageService.getItem('username'));
          this.loggedUserDataSubject$.next(this.getLoggedUserData());
        })
      );
  }

  logout(): void {
    this.storageService.removeItem('authToken');
    this.storageService.setItem('joined', false);
    this.socketService.leave(this.storageService.getItem('username'));
    this.loggedUserDataSubject$.next(null);
  }


  getLoggedUserData(): UserDTO {
    const authToken: string = this.storageService.getItem('authToken');

    if (authToken && this.jwtHelperService.isTokenExpired(authToken)) {
      this.storageService.removeItem('authToken');
      this.storageService.setItem('joined', false);

      return null;
    }

    return authToken ? this.jwtHelperService.decodeToken(authToken) : null;
  }
}
