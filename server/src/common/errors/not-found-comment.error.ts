import { HomeWorldApplicationError } from './home-world-application.error';

export class NotFoundCommentError extends HomeWorldApplicationError {
  constructor(message: string = 'Not found comment!') {
    super(message, 404);
  }
}
