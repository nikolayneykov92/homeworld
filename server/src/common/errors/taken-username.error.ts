import { HomeWorldApplicationError } from './home-world-application.error';

export class TakenUsernameError extends HomeWorldApplicationError {
  constructor(message: string = 'This username is already taken!') {
    super(message, 400);
  }
}
