export class UserDTO {
  id: number;
  username: string;
  email: string;
  photoLink: string;
  role: string;
  isFollowed: boolean;
  followersCount: number;
  followingCount: number;
  createdPostsCount: number;
}
