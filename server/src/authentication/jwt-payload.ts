import { UserRole } from '../common/enums/user-role.enum';

export interface JwtPayload {
  id: number;
  username: string;
  email: string;
  photoLink: string;
  role: UserRole;
}
