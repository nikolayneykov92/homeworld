import { HomeWorldApplicationError } from './home-world-application.error';

export class InvalidImageFormat extends HomeWorldApplicationError {
  constructor(message: string = 'Invalid image format! Allowed image formats are: .png, .jpg, .gif, .jpeg...') {
    super(message, 400);
  }
}
