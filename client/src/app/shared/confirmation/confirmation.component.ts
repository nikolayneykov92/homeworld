import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent {
  @Input() message: string;
  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  accept(): void {
    this.confirm.emit(true);
  }

  deny(): void {
    this.confirm.emit(false);
  }
}
