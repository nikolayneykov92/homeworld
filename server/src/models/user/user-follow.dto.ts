import { IsBoolean, IsPositive, IsInt } from 'class-validator';

export class UserFollowDTO {
  @IsInt()
  @IsPositive()
  userId: number;

  @IsBoolean()
  follow: boolean;
}
