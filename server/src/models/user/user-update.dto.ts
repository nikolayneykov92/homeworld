import { IsString, Length, IsNotEmpty, IsEmail, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserUpdateDTO {
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty({ required: false })
  username: string;

  @IsOptional()
  @IsEmail()
  @IsNotEmpty()
  @ApiModelProperty({ required: false })
  email: string;

  @IsOptional()
  @IsString()
  @Length(0, 1000)
  @ApiModelProperty({ required: false })
  description: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty({ required: false })
  password: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty({ required: false })
  newPassword: string;
}
