import { UserDTO } from '../user/user.dto';

export class PostFullDTO {
  id: number;
  title: string;
  description: string;
  status: string;
  photoLink: string;
  creationDate: Date;
  creator: UserDTO;
  likes: number;
  liked: boolean;
  location: string;
}
