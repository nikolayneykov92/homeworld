import { UserDTO } from './../../models/user/user.dto';
import { UsersService } from './../services/users.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class UsersResolver implements Resolve<any> {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<UserDTO[]> {
    return this.usersService.findUsers('', 0, 10)
      .pipe(
        catchError(() => {
          return EMPTY;
        })
      );
  }
}
