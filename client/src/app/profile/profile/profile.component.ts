import { UpdateProfileConfirmationComponent } from './../update-profile-confirmation/update-profile-confirmation.component';
import { ConfirmationComponent } from './../../shared/confirmation/confirmation.component';
import { NotificationService } from './../../core/services/notification.service';
import { UsersService } from './../../core/services/users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../models/user/user.dto';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  profilePhoto = [];
  likedPostsAlbum = [];
  createdPostsAlbum = [];

  loggedUserData: UserDTO;
  user: UserDTO;
  followers: UserDTO[];
  following: UserDTO[];
  isOpenSettings = false;
  createdPosts = [];
  likedPosts = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
    private readonly modalService: NgbModal,
    private readonly spinner: NgxSpinnerService,
    private readonly router: Router,
    private readonly lightbox: Lightbox,
  ) { }

  openAlbum(album: any, index: number): void {
    this.lightbox.open(album, index);
  }

  openSettings(): void {
    if (this.loggedUserData.id === this.user.id) {
      this.isOpenSettings = !this.isOpenSettings;
    }
  }

  followUser(): void {
    this.usersService.followUser(this.loggedUserData.id, this.user.id)
      .subscribe(({ userFollowed, userFollower }) => {
        this.user = userFollowed;
        this.followers = [userFollower, ...this.followers];
        this.notificationService.success(`You have started following ${userFollowed.username}.`);
      });
  }

  unFollowUser(): void {
    this.usersService.unFollowUser(this.loggedUserData.id, this.user.id)
      .subscribe(({ userFollowed, userFollower }) => {
        this.user = userFollowed;
        this.followers = this.followers.filter((u: UserDTO) => u.id !== userFollower.id);
        this.notificationService.warning(`You are no longer following ${userFollowed.username}.`);
      });
  }

  confirmDeleteUser(): void {
    const modalRef = this.modalService.open(ConfirmationComponent);
    modalRef.componentInstance.message = 'Are you sure you want to delete your profile?';

    modalRef
      .componentInstance
      .confirm
      .subscribe((confirmed: boolean) => {
        if (confirmed && this.user.id === this.loggedUserData.id) {
          this.usersService.deleteUser(this.loggedUserData.id)
            .subscribe((data: UserDTO) => {
              modalRef.close();
              this.notificationService.warning(`Goodbye ${data.username}. We will miss you...`);
              this.router.navigate(['/home-world']);
            });
        } else {
          modalRef.close();
        }
      });
  }

  confirmUpdateUser(updateUserInfo): void {
    const modalRef = this.modalService.open(UpdateProfileConfirmationComponent);
    modalRef.componentInstance.message = 'Are you sure you want to update your account?';

    modalRef
      .componentInstance
      .confirm
      .subscribe((currentPassword: string) => {
        if (
          currentPassword &&
          (this.user.id === this.loggedUserData.id || this.loggedUserData.role === 'Admin')
        ) {
          this.spinner.show();
          const username = updateUserInfo.newUsername || this.loggedUserData.username;
          const email = updateUserInfo.newEmail || this.loggedUserData.email;
          const loginPassword = updateUserInfo.newPassword || currentPassword;
          const photo = updateUserInfo.photo || null;
          const formData = new FormData();

          if (updateUserInfo.newPassword) {
            formData.append('newPassword', updateUserInfo.newPassword);
          }

          formData.append('username', username);
          formData.append('email', email);
          formData.append('photo', photo);
          formData.append('password', currentPassword);

          this.usersService.updateUser(this.user.id, formData)
            .subscribe(
              (data: UserDTO) => {
                this.user = data;
                this.profilePhoto = [{ src: data.photoLink, caption: data.username, thumb: '' }];

                modalRef.close();

                if (this.loggedUserData.role === 'User') {
                  this.authService.login({ username, password: loginPassword })
                    .subscribe(
                      () => {
                        this.notificationService.success(`You have updated your account successfully!`);
                        this.spinner.hide();
                      },
                      ({ error }) => {
                        this.notificationService.error('Invalid Credentials!');
                        this.spinner.hide();
                      }
                    );
                } else {
                  this.spinner.hide();
                }
              },
              ({ error }) => {
                this.notificationService.error('Invalid Credentials!');
                modalRef.close();
                this.spinner.hide();
              }
            );
        } else {
          modalRef.close();
        }
      });
  }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data: UserDTO) => {
        this.loggedUserData = data;
      });

    this.route.data
      .subscribe(({ data }) => {
        this.profilePhoto = [{ src: data.user.photoLink, caption: data.user.username, thumb: '' }];
        this.user = data.user;
        this.likedPosts = data.likedPosts;
        this.createdPosts = data.createdPosts;
        this.following = data.following;
        this.followers = data.followers;

        this.likedPosts.forEach(p => {
          this.likedPostsAlbum.push({ src: p.photoLink, caption: p.title });
        });

        this.createdPosts.forEach(p => {
          this.createdPostsAlbum.push({ src: p.photoLink, caption: p.title });
        });
      });

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }
}
