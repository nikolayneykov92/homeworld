import { fakeUsersInfo } from './fake.users-info';
import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Role } from '../entities/role.entity';
import { User } from '../entities/user.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import { Post } from '../entities/post.entity';
import { fakePostsInfo } from './fake.post-photos';
import { PostStatus } from '../../common/enums/post-status.enum';

const main = async () => {
  console.log('Seeding data started...');

  const connection = await createConnection();

  const rolesRepository = connection.getRepository(Role);
  const usersRepository = connection.getRepository(User);
  const postsRepository = connection.getRepository(Post);

  const adminRole = await rolesRepository.save({ type: UserRole.Admin });
  const userRole = await rolesRepository.save({ type: UserRole.User });

  const getFakeUser = async (username: string, role: Role, photoLink: string): Promise<User> => {
    const fakeUser: User = usersRepository.create();

    fakeUser.username = username;
    fakeUser.email = username + '@email.com';
    fakeUser.role = role;
    fakeUser.password = await bcrypt.hash('123', 10);
    fakeUser.photoLink = photoLink || 'https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png';
    fakeUser.createdPosts = Promise.resolve([]);
    fakeUser.createdComments = Promise.resolve([]);
    fakeUser.commentLikes = Promise.resolve([]);
    fakeUser.followers = [];
    fakeUser.following = [];

    return fakeUser;
  };

  const getFakePost = async (post: Partial<Post>, user: User): Promise<Post> => {
    const fakePost: Post = postsRepository.create();

    fakePost.title = post.title;
    fakePost.photoLink = post.photoLink;
    fakePost.location = post.location;
    fakePost.status = PostStatus.public;
    fakePost.description = '';
    fakePost.photoDeleteHash = '';
    fakePost.creator = Promise.resolve(user);
    fakePost.likes = Promise.resolve([]);
    fakePost.creationDate = new Date();

    return fakePost;
  };

  const fakeUsers = fakeUsersInfo.map((u) => getFakeUser(u.username, userRole, u.photoLink));

  const seededUsers = await usersRepository.save(await Promise.all(fakeUsers));

  await usersRepository.save(seededUsers.map((user, index) => {
    const randomIndex = Math.floor(Math.random() * seededUsers.length);
    user.following = seededUsers.slice(randomIndex).filter((u, i) => i !== index);

    return user;
  }));

  const fakePosts = await Promise.all(
    fakePostsInfo.map((p) => getFakePost(p, seededUsers[0])),
  );

  await postsRepository.save(fakePosts);

  await connection.close();

  console.log('Data seeded successfully');
};

main().catch(console.error);
