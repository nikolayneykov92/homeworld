import { AnonymousGuard } from './core/guards/anonymous.guard';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { UsersResolver } from './core/resolvers/users.resolver';
import { PostsResolver } from './core/resolvers/posts.resolver';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home-world', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent, canActivate: [AnonymousGuard] },
  { path: 'login', component: LoginComponent, canActivate: [AnonymousGuard] },
  { path: 'home-world', loadChildren: './posts/posts.module#PostsModule', resolve: { data: PostsResolver } },
  { path: 'people', loadChildren: './users/users.module#UsersModule', resolve: { data: UsersResolver } },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule', canActivate: [AuthGuard] },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
