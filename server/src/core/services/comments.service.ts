import { CommentLike } from './../../database/entities/comment-like.entity';
import { PostLike } from './../../database/entities/post-like.entity';
import { NotAuthorizedError } from './../../common/errors/not-authorized.error';
import { UserSessionDTO } from './../../models/user/user-session.dto';
import { UserSession } from './../../decorators/user-session.decorator';
import { CommentDTO } from './../../models/comments/comment.dto';
import { NotFoundCommentError } from './../../common/errors/not-found-comment.error';
import { CommentUpdateDTO } from './../../models/comments/comment-update.dto';
import { NotFoundUserError } from './../../common/errors/not-found-user.error';
import { NotFoundPostError } from './../../common/errors/not-found-post.error';
import { CommentCreateDTO } from './../../models/comments/comment-create.dto';
import { Comment } from './../../database/entities/comment.entity';
import { MapperService } from './mapper.service';
import { Post } from '../../database/entities/post.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../database/entities/user.entity';
import { UserRole } from '../../common/enums/user-role.enum';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Comment) private readonly commentsRepository: Repository<Comment>,
    private readonly mapper: MapperService,
  ) { }

  async findComments(postId: number, loggedUserId: number, options: any): Promise<CommentDTO[]> {
    const foundComments: Comment[] = await this.commentsRepository.find({
      where: { post: { id: postId, isDeleted: false }, isDeleted: false },
      relations: ['creator', 'likes'],
      order: { creationDate: 'DESC' },
      take: options.take || '10',
      skip: options.skip || '0',
    });

    const foundUserCommentLikes = loggedUserId === -1 ?
      null
      : (await this.usersRepository.findOne({
        where: { id: loggedUserId },
        relations: ['commentLikes'],
      }))['__commentLikes__'];

    if (foundUserCommentLikes) {
      return foundComments
        .map((c: Comment) => {
          const liked: boolean = c['__likes__']
            .some((like: CommentLike) => {
              return foundUserCommentLikes
                .some((commentLike: CommentLike) =>
                  commentLike.id === like.id && commentLike.liked,
                );
            });

          return {
            ...this.mapper.toCommentDTO({ ...c }),
            creator: this.mapper.toUserDTO(c['__creator__']),
            liked,
          };
        });
    }

    return foundComments
      .map((c: Comment) => {
        return {
          ...this.mapper.toCommentDTO({ ...c, creator: c['__creator__'] }),
          liked: false,
        };
      });
  }

  async createComment(
    postId: number,
    userId: number,
    comment: CommentCreateDTO,
  ): Promise<CommentDTO> {
    const [foundPost, foundUser] = await Promise.all([
      this.postsRepository.findOne({ id: postId, isDeleted: false }),
      this.usersRepository.findOne({ id: userId, isDeleted: false }),
    ]);

    if (!foundPost) {
      throw new NotFoundPostError();
    }

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const newComment: Comment = this.commentsRepository.create(comment);
    newComment.post = Promise.resolve(foundPost);
    newComment.creator = Promise.resolve(foundUser);
    newComment.creationDate = new Date();
    newComment.likes = Promise.resolve([]);

    const createdComment: Comment = await this.commentsRepository.save(newComment);

    return {
      ...this.mapper.toCommentDTO({
        ...createdComment, creator: createdComment['__creator__'],
      }),
      liked: false,
    };
  }

  async updateComment(
    commentId: number,
    comment: CommentUpdateDTO,
    userSession: UserSessionDTO,
  ) {
    const foundComment: Comment = await this.commentsRepository
      .findOne({ where: { id: commentId, isDeleted: false }, relations: ['creator'] });

    if (!foundComment) {
      throw new NotFoundCommentError();
    }

    const creatorId: number = foundComment['__creator__'].id;

    const canUpdate: boolean = creatorId === userSession.id || userSession.role === UserRole.Admin;

    if (!canUpdate) {
      throw new NotAuthorizedError();
    }

    const updatedComment: Comment = { ...foundComment, ...comment };

    await this.commentsRepository.update(commentId, comment);

    return this.mapper.toCommentDTO(updatedComment);
  }

  async deleteComment(commentId: number, userSession: UserSessionDTO) {
    const foundComment: Comment = await this.commentsRepository
      .findOne({ where: { id: commentId, isDeleted: false }, relations: ['creator'] });

    if (!foundComment) {
      throw new NotFoundCommentError();
    }

    const creatorId: number = foundComment['__creator__'].id;

    const canDelete: boolean = creatorId === userSession.id || userSession.role === UserRole.Admin;

    if (!canDelete) {
      throw new NotAuthorizedError();
    }

    await this.commentsRepository.update(commentId, { isDeleted: true });

    return this.mapper.toCommentDTO(foundComment);
  }
}
