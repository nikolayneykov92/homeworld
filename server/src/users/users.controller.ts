import { UserSessionDTO } from './../models/user/user-session.dto';
import { UserSession } from './../decorators/user-session.decorator';
import { UserFollowDTO } from './../models/user/user-follow.dto';
import { UserGuard } from '../guards/user.guard';
import { UserUpdateDTO } from '../models/user/user-update.dto';
import { UserDTO } from '../models/user/user.dto';
import { HomeWorldApplicationExceptionFilter } from '../filters/home-world-application-exception.filter';
import { UserCreateDTO } from '../models/user/user-create.dto';
import { ValidationPipe } from '@nestjs/common/pipes/validation.pipe';
import { FileInterceptor } from '@nestjs/platform-express';

import {
  Post,
  Get,
  Put,
  Delete,
  Controller,
  Body,
  UseFilters,
  UseGuards,
  Param,
  ParseIntPipe,
  Query,
  UseInterceptors,
  UploadedFile,
  Patch,
} from '@nestjs/common';

import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
import { SearchOptionsDTO } from '../models/search-options.dto';
import { ApiUseTags, ApiBearerAuth, ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { PostDTO } from '../models/post/post.dto';

@Controller('api/users')
@UseFilters(HomeWorldApplicationExceptionFilter)
@ApiUseTags('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Get()
  async findUsers(
    @Query(new ValidationPipe({ transform: true, whitelist: true })) options: SearchOptionsDTO,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<UserDTO[]> {
    return await this.usersService.findUsers(options, loggedUser ? loggedUser.id : -1);
  }

  @Get(':userId')
  async findUserById(
    @Param('userId', new ParseIntPipe()) userId: number,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<UserDTO> {
    return await this.usersService.findUserById(userId, loggedUser ? loggedUser.id : -1);
  }

  @Post()
  async createUser(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserCreateDTO,
  ): Promise<UserDTO> {
    return await this.usersService.createUser(user);
  }

  @Put(':userId')
  @UseGuards(AuthGuard())
  @UseInterceptors(FileInterceptor('photo'))
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'photo', required: false, description: 'profile photo' })
  async updateUser(
    @Param('userId', new ParseIntPipe()) userId: number,
    @UserSession() userSession: UserSessionDTO,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserUpdateDTO,
    @UploadedFile() photo: any,
  ): Promise<UserDTO> {
    return await this.usersService.updateUser(userId, userSession, user, photo);
  }

  @Delete(':userId')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async deleteUser(
    @Param('userId', new ParseIntPipe()) userId: number,
    @UserSession() userSession: UserSessionDTO,
  ): Promise<UserDTO> {
    return await this.usersService.deleteUser(userId, userSession);
  }

  @Post(':userId/following')
  @UseGuards(AuthGuard(), UserGuard)
  @ApiBearerAuth()
  async followUser(
    @Param('userId', new ParseIntPipe()) userId: number,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserFollowDTO,
    @UserSession() userSession: UserSessionDTO,
  ): Promise<{ userFollower: UserDTO, userFollowed: UserDTO }> {
    return await this.usersService.followUser(userId, userSession.id, user);
  }

  @Get(':userId/following')
  async getUserFollowing(
    @Param('userId', new ParseIntPipe()) userId: number,
  ): Promise<UserDTO[]> {
    return this.usersService.getUserFollowing(userId);
  }

  @Get(':userId/followers')
  async getUserFollowers(
    @Param('userId', new ParseIntPipe()) userId: number,
  ): Promise<UserDTO[]> {
    return this.usersService.getUserFollowers(userId);
  }

  @Get(':userId/posts')
  async getUserPosts(
    @Param('userId', new ParseIntPipe()) userId: number,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<PostDTO[]> {
    return await this.usersService.getUserPosts(userId, loggedUser ? loggedUser.id : -1);
  }

  @Get(':userId/posts/liked')
  async getUserLikedPosts(
    @Param('userId', new ParseIntPipe()) userId: number,
    @UserSession() loggedUser: UserSessionDTO,
  ): Promise<PostDTO[]> {
    return await this.usersService.getUserLikedPosts(userId, loggedUser ? loggedUser.id : -1);
  }
}
