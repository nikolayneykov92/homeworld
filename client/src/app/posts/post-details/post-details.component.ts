import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { PostFullDTO } from '../../models/post/post-full.dto';
import { CommentCreateDTO } from '../../models/comment/comment-create.dto';
import { CommentDTO } from '../../models/comment/comment.dto';
import { UserDTO } from '../../models/user/user.dto';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-post-details-component',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss'],
})
export class PostDetailsComponent {
  @Input() loggedUserData: UserDTO;
  @Input() post: PostFullDTO;
  @Input() comments: CommentDTO[] = [];

  @Output() detailsClose: EventEmitter<void> = new EventEmitter<void>();

  @Output() postEdit: EventEmitter<PostFullDTO> = new EventEmitter<PostFullDTO>();
  @Output() postDelete: EventEmitter<PostFullDTO> = new EventEmitter<PostFullDTO>();
  @Output() postLike: EventEmitter<PostFullDTO> = new EventEmitter<PostFullDTO>();

  @Output() commentCreate: EventEmitter<CommentCreateDTO> = new EventEmitter<CommentCreateDTO>();
  @Output() commentEdit: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();
  @Output() commentDelete: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();
  @Output() commentLike: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();

  commentContent = '';
  postPhoto = [];

  constructor(
    private readonly lightbox: Lightbox,
  ) { }

  openPostPhoto(): void {
    this.lightbox.open([{ src: this.post.photoLink, caption: this.post.title, thumb: '' }], 0);
  }

  editPost(): void {
    this.postEdit.emit(this.post);
  }

  deletePost(): void {
    this.postDelete.emit(this.post);
  }

  likePost(): void {
    this.postLike.emit(this.post);
  }

  createComment(): void {
    this.commentCreate.emit({ content: this.commentContent, postId: this.post.id, });
    this.commentContent = '';
  }

  editComment(commentToEdit: CommentDTO): void {
    this.commentEdit.emit({ ...commentToEdit, postId: this.post.id });
  }

  deleteComment(commentToDelete: CommentDTO): void {
    this.commentDelete.emit({ ...commentToDelete, postId: this.post.id });
  }

  likeComment(commentToLike: CommentDTO): void {
    this.commentLike.emit({ ...commentToLike, postId: this.post.id });
  }

  closeDetails() {
    this.detailsClose.emit();
  }
}
