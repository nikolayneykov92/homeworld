import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostInfoComponent } from './post-info/post-info.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PostCreateComponent } from './post-create/post-create.component';
import { PostEditComponent } from './post-edit/post-edit.component';
import { CommentEditComponent } from './comment-edit/comment-edit-component';
import { PostsRoutingModule } from './posts.routing';

@NgModule({
  declarations: [
    PostsListComponent,
    PostInfoComponent,
    PostDetailsComponent,
    PostCreateComponent,
    PostEditComponent,
    CommentEditComponent,
  ],
  imports: [
    SharedModule,
    PostsRoutingModule,
  ],
  entryComponents: [
    PostDetailsComponent,
    PostCreateComponent,
    PostEditComponent,
    CommentEditComponent,
  ],
  exports: [PostsListComponent],
  providers: [
    NgbActiveModal
  ]
})
export class PostsModule { }
