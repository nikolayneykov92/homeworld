import { HomeWorldApplicationError } from '../common/errors/home-world-application.error';
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';

@Catch(HomeWorldApplicationError)
export class HomeWorldApplicationExceptionFilter implements ExceptionFilter {
  catch(exception: HomeWorldApplicationError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    response.status(exception.code).json({
      status: exception.code,
      timestamp: new Date().toISOString(),
      method: request.method,
      path: request.url,
      message: exception.message,
    });
  }
}
