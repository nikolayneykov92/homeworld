import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { UserRole } from '../common/enums/user-role.enum';

@Injectable()
export class UserGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();

    const { user } = request;

    const isUser = user && user.role === UserRole.User;

    return isUser;
  }
}
