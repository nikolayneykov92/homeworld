export class PostDTO {
  id: number;
  title: string;
  description: string;
  status: string;
  photoLink: string;
  creationDate: Date;
  liked: boolean;
  location: string;
}
