import { UserDTO } from '../user/user.dto';

export class CommentDTO {
    id: number;
    postId: number;
    content: string;
    creator: UserDTO;
    likes: number;
    liked: boolean;
    creationDate: Date;
}
