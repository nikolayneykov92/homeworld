import { Transform, Exclude } from 'class-transformer';
import { IsString, Length, IsNotEmpty, IsEmail, IsNumber, IsPositive, IsOptional, IsBoolean } from 'class-validator';
import { Role } from '../../database/entities/role.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserDTO {
  @IsNumber()
  @IsPositive()
  @ApiModelProperty()
  id: number;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty()
  username: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiModelProperty()
  email: string;

  @Transform((role: Role) => UserRole[role.type])
  @ApiModelProperty()
  role: UserRole;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  photoLink: string;

  @IsBoolean()
  isFollowed: boolean;

  @Exclude()
  photoDeleteHash: string;

  @Exclude()
  isDeleted: boolean;

  @Exclude()
  password: string;

  @Exclude()
  followers: any;

  @Exclude()
  following: any;
}
