import { IsString, Length, IsNotEmpty, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserCreateDTO {
  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty()
  username: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiModelProperty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty()
  password: string;
}
