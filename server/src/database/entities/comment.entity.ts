import { CommentLike } from './comment-like.entity';
import { User } from './user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Post } from './post.entity';

@Entity('comments')
export class Comment {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, length: 200 })
  content: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @ManyToOne(type => User, user => user.createdComments)
  creator: Promise<User>;

  @ManyToOne(type => Post, post => post.comments)
  post: Promise<Post>;

  @OneToMany(type => CommentLike, commentLike => commentLike.comment)
  likes: Promise<CommentLike[]>;

  @Column({ type: 'datetime', default: '01.01.2000' })
  creationDate: Date;
}
