import { createParamDecorator } from '@nestjs/common';

export const UserSession = createParamDecorator((_, req) => {
  const { authorization } = req.headers;
  const token = authorization ? authorization.replace('Bearer ', '') : 'null';

  return token !== 'null' ? JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString()) : null;
});
