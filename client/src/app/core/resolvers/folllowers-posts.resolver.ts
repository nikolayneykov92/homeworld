import { UserDTO } from './../../models/user/user.dto';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PostsService } from '../services/posts.service';

@Injectable()
export class FolowersPostsResolver implements Resolve<UserDTO> {
    constructor(
        private readonly postsService: PostsService,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<any> {
        const userId = +route.paramMap.get('userId');

        return this.postsService.findFollowersPosts(userId, 0, 15)
            .pipe(
                catchError(() => {
                    return EMPTY;
                })
            );
    }
}
