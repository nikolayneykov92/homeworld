import { UserDTO } from './../../models/user/user.dto';
import { UsersService } from './../services/users.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class LikedPostsResolver implements Resolve<UserDTO> {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> {
    const userId = +route.paramMap.get('userId');

    return this.usersService.findUserLikedPosts(userId)
      .pipe(
        catchError(() => {
          return EMPTY;
        })
      );
  }
}
