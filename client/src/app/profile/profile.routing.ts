import { ProfileResolver } from './../core/resolvers/profile.resolver';
import { ProfileComponent } from './profile/profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: ':userId', component: ProfileComponent, resolve: { data: ProfileResolver } },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
  ],
})
export class ProfileRoutingModule { }
