export class PostUpdateDTO {
    title: string;
    description: string;
    status: string;
  }
