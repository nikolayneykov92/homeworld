import { Comment } from './comment.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('comment_likes')
export class CommentLike {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'boolean', default: false })
  liked: boolean;

  @ManyToOne(type => User, user => user.commentLikes)
  user: Promise<User>;

  @ManyToOne(type => Comment, comment => comment.likes)
  comment: Promise<Comment>;
}
