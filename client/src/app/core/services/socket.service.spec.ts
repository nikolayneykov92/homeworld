import { SocketIoModule, Socket } from 'ngx-socket-io';
import { SocketService } from './socket.service';
import { TestBed } from '@angular/core/testing';

describe('SocketService', () => {
  const fakeUsername = 'test';

  let socket;
  let socketService: SocketService;

  beforeEach(async () => {
    socket = {
      emit() { },
      fromEvent() { },
    };

    TestBed.configureTestingModule({
      imports: [
        SocketIoModule.forRoot({
          url: '',
          options: {},
        }),
      ],
      providers: [SocketService]
    })
      .overrideProvider(Socket, { useValue: socket });

    socketService = TestBed.get(SocketService);
  });

  it('should be created', () => {
    expect(socketService).toBeTruthy();
  });

  it('socketService.join() should call socket.emit() with the correct parameters', () => {
    const emitSpy = spyOn(socket, 'emit');

    socketService.join(fakeUsername);

    expect(emitSpy).toHaveBeenCalledTimes(1);
    expect(emitSpy).toHaveBeenCalledWith('join', fakeUsername);
  });

  it('socketService.leave() should call socket.emit() with the correct parameters', () => {
    const emitSpy = spyOn(socket, 'emit');

    socketService.leave(fakeUsername);

    expect(emitSpy).toHaveBeenCalledTimes(1);
    expect(emitSpy).toHaveBeenCalledWith('leave', fakeUsername);
  });

  it('socketService.sendNotification() should call socket.emit() with the correct parameters', () => {
    const emitSpy = spyOn(socket, 'emit');

    socketService.sendNotification(fakeUsername);

    expect(emitSpy).toHaveBeenCalledTimes(1);
    expect(emitSpy).toHaveBeenCalledWith('notification', fakeUsername);
  });

  it('socketService.receiveNotification() should call socket.fromEvent() with the correct parameters', () => {
    const fromEventSpy = spyOn(socket, 'fromEvent');

    socketService.receiveNotification();

    expect(fromEventSpy).toHaveBeenCalledTimes(1);
    expect(fromEventSpy).toHaveBeenCalledWith('notification');
  });
});
