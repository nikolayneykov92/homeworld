import { Component, Input } from '@angular/core';
import { UserDTO } from '../../models/user/user.dto';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent {
  @Input() followers: UserDTO[];
}
