import { ConfirmationComponent } from './../../shared/confirmation/confirmation.component';
import { CommentEditComponent } from '../comment-edit/comment-edit-component';
import { CommentDTO } from './../../models/comment/comment.dto';
import { CommentCreateDTO } from './../../models/comment/comment-create.dto';
import { NgxSpinnerService } from 'ngx-spinner';
import { PostDTO } from './../../models/post/post.dto';
import { PostsService } from './../../core/services/posts.service';
import { Subscription, zip } from 'rxjs';
import { PostCreateComponent } from './../post-create/post-create.component';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PostDetailsComponent } from '../post-details/post-details.component';
import { PostFullDTO } from '../../models/post/post-full.dto';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../models/user/user.dto';
import { PostUpdateDTO } from '../../models/post/post-update.dto';
import { NotificationService } from '../../core/services/notification.service';
import { PostEditComponent } from '../post-edit/post-edit.component';
import { CommentsService } from '../../core/services/comments.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { NgxMasonryOptions } from 'ngx-masonry';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit, OnDestroy {
  masonryOptions: NgxMasonryOptions = {
    transitionDuration: '0.4s',
    gutter: 20,
    resize: true,
    initLayout: true,
    fitWidth: true,
    originTop: true,
  };

  posts: PostDTO[];

  private postDetailsModal: NgbModalRef;
  private loggedUserSubscription: Subscription;
  loggedUserData: UserDTO;

  routeType: string;
  take = 6;
  skip = 0;
  limit = 0;

  constructor(
    private readonly modalService: NgbModal,
    private readonly spinner: NgxSpinnerService,
    private readonly postsService: PostsService,
    private readonly commentsService: CommentsService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) { }

  showMorePosts(): void {
    if (this.skip <= this.limit) {
      this.postsService.findPosts(this.skip, this.take)
        .subscribe((data: PostDTO[]) => {
          this.posts = [...this.posts, ...data];
          this.skip += this.take;
          this.limit = this.posts.length + 1;
        });
    }
  }

  showPostDetails(postToShow: PostFullDTO): void {
    if (!this.loggedUserData) {
      this.notificationService.info('Please Login to see this post :)');
      return;
    }

    zip(
      this.postsService.findPostById(postToShow.id),
      this.commentsService.findComments(postToShow.id),
    ).subscribe(([post, comments]) => {
      this.postDetailsModal = this.modalService.open(PostDetailsComponent);

      this.postDetailsModal.componentInstance.loggedUserData = this.loggedUserData;
      this.postDetailsModal.componentInstance.post = post;
      this.postDetailsModal.componentInstance.comments = comments;

      this.postDetailsModal.componentInstance.detailsClose
        .subscribe(() => {
          this.postDetailsModal.close();
        });

      this.postDetailsModal.componentInstance.postEdit
        .subscribe((postToEdit: PostDTO) => {
          this.editPost(postToEdit);
        });

      this.postDetailsModal.componentInstance.postDelete
        .subscribe((postToDelete: PostDTO) => {
          this.deletePost(postToDelete);
        });

      this.postDetailsModal.componentInstance.postLike
        .subscribe((postToLike: PostDTO) => {
          this.likePost(postToLike);
        });

      this.postDetailsModal.componentInstance.commentCreate
        .subscribe((comment: CommentCreateDTO) => {
          this.createComment(comment);
        });

      this.postDetailsModal.componentInstance.commentEdit
        .subscribe((commentToEdit: CommentDTO) => {
          this.editComment(commentToEdit);
        });

      this.postDetailsModal.componentInstance.commentDelete
        .subscribe((commentToDelete: CommentDTO) => {
          this.deleteComment(commentToDelete);
        });

      this.postDetailsModal.componentInstance.commentLike
        .subscribe((commentToLike: CommentDTO) => {
          this.likeComment(commentToLike);
        });
    });
  }

  createPost(): void {

    const postCreateModal: NgbModalRef = this.modalService.open(PostCreateComponent);

    postCreateModal.componentInstance.postCreate
      .subscribe((post: FormData) => {
        this.spinner.show();

        this.postsService.createPost(post)
          .subscribe((data: PostDTO) => {
            postCreateModal.close();

            if (
              (this.routeType === 'home-world' && data.status === 'public') ||
              this.routeType === 'created'
            ) {
              this.posts = [data];
              this.skip = 1;
              this.limit = this.posts.length + 1;
            }

            this.spinner.hide();
          });
      });
  }

  editPost(post: PostDTO): void {
    const postEditModal: NgbModalRef = this.modalService.open(PostEditComponent);
    postEditModal.componentInstance.post = post;

    postEditModal.componentInstance.postEdit
      .subscribe((updatedPost: PostUpdateDTO) => {
        this.postsService
          .editPost(post.id, updatedPost)
          .subscribe((editedPost: PostFullDTO) => {
            this.postDetailsModal.componentInstance.post = {
              ...this.postDetailsModal.componentInstance.post,
              ...editedPost,
            };
            postEditModal.close();

            if (editedPost.status === 'private' && this.routeType === 'home-world') {
              this.posts = this.posts.filter(p => p.id !== editedPost.id);
              this.postDetailsModal.close();
            }

            this.notificationService.success('Post updated successfully');
          });
      });
  }

  deletePost(postToDelete: PostDTO): void {
    const confirmationModal = this.modalService.open(ConfirmationComponent);
    confirmationModal.componentInstance.message = 'Are you sure you want to delete your post?';

    confirmationModal.componentInstance.confirm
      .subscribe((confirmed: boolean) => {
        if (!confirmed) {
          confirmationModal.close();
          return;
        }

        this.spinner.show();

        this.postsService.deletePost(postToDelete.id)
          .subscribe((deletedPost: PostDTO) => {
            this.posts = this.posts.filter((p: PostDTO) => p.id !== deletedPost.id);
            confirmationModal.close();
            this.postDetailsModal.close();
            this.spinner.hide();
            this.notificationService.success('Post deleted');
          });
      });
  }

  likePost(postToLike: PostDTO): void {
    this.postsService.likePost(postToLike.id, { liked: !postToLike.liked })
      .subscribe((data) => {
        this.postDetailsModal.componentInstance.post.liked = data.liked;
        this.postDetailsModal.componentInstance.post.likes += data.liked ? 1 : -1;
      });
  }

  createComment(comment: CommentCreateDTO): void {
    this.commentsService.createComment(comment.postId, comment)
      .subscribe((createdComment: CommentDTO) => {
        this.postDetailsModal.componentInstance.comments = [
          createdComment,
          ...this.postDetailsModal.componentInstance.comments
        ];
      });
  }

  editComment(commentToEdit: CommentDTO): void {
    const commentEditModal = this.modalService.open(CommentEditComponent);
    commentEditModal.componentInstance.commentToEdit = commentToEdit;

    commentEditModal.componentInstance.commentEdit
      .subscribe((editedComment: CommentDTO) => {
        this.commentsService
          .editComment(editedComment.postId, editedComment.id, editedComment)
          .subscribe((comment: CommentDTO) => {
            const commentIndex = this.postDetailsModal.componentInstance.comments
              .findIndex((c: CommentDTO) => c.id === comment.id);

            if (commentIndex !== -1) {
              this.postDetailsModal.componentInstance.comments[commentIndex].content = comment.content;
            }

            commentEditModal.close();
          });
      });
  }

  deleteComment(commentToDelete: CommentDTO): void {
    const confirmationModal = this.modalService.open(ConfirmationComponent);
    confirmationModal.componentInstance.message = 'Are you sure you want to delete your comment?';

    confirmationModal.componentInstance.confirm
      .subscribe((confirmed: boolean) => {
        if (!confirmed) {
          confirmationModal.close();
          return;
        }

        this.commentsService.deleteComment(commentToDelete.postId, commentToDelete.id)
          .subscribe((deletedComment: CommentDTO) => {
            this.postDetailsModal.componentInstance.comments =
              this.postDetailsModal.componentInstance.comments
                .filter((c: CommentDTO) => c.id !== deletedComment.id);

            confirmationModal.close();
            this.notificationService.success('Comment deleted!');
          });
      });
  }

  likeComment(commentToLike: CommentDTO): void {
    this.commentsService.likeComment(commentToLike.postId, commentToLike.id, { liked: !commentToLike.liked })
      .subscribe((like: any) => {
        const commentIndex = this.postDetailsModal.componentInstance.comments
          .findIndex((c: CommentDTO) => c.id === commentToLike.id);

        if (commentIndex !== -1) {
          this.postDetailsModal.componentInstance.comments[commentIndex].liked = like.liked;
        }
      });
  }

  selectPost() {
    if (this.routeType === 'home-world') {
      this.router.navigate(['/home-world']);
    } else {
      this.router.navigate(['/home-world', 'people', this.loggedUserData.id, 'posts', this.routeType]);
    }
  }

  ngOnInit(): void {
    this.routeType = this.router.url.split('/').pop();

    this.loggedUserSubscription = this.authService.loggedUserData$
      .subscribe((data: UserDTO) => {
        this.loggedUserData = data;
      });

    this.route.data
      .subscribe(({ data }) => {
        this.posts = data;
        this.skip = data.length;
        this.limit = data.length + 1;
      });

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }

      window.scrollTo(0, 0);
    });
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();

    if (this.postDetailsModal) {
      this.postDetailsModal.close();
    }
  }
}
