import { IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class PostLikeDTO {
  @ApiModelProperty()
  id: number;

  @IsBoolean()
  @ApiModelProperty()
  liked: boolean;
}
