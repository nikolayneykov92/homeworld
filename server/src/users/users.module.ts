import { PostLike } from './../database/entities/post-like.entity';
import { NotificationModule } from './../notification/notification.module';
import { CoreModule } from './../core/core.module';
import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/role.entity';
import { ConfigModule } from '../config/config.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { Post } from '../database/entities/post.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Post, PostLike]),
    AuthenticationModule,
    ConfigModule,
    CoreModule,
    NotificationModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule { }
