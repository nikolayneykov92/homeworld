import { PostLike } from './../database/entities/post-like.entity';
import { PostUpdateDTO } from './../models/post/post-update.dto';
import { NotFoundPostError } from './../common/errors/not-found-post.error';
import { NotFoundUserError } from './../common/errors/not-found-user.error';
import { MapperService } from './../core/services/mapper.service';
import { PostCreateDTO } from './../models/post/post-create.dto';
import { PhotoStorageService } from './../core/services/photo-storage.service';
import { Post } from './../database/entities/post.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { PostDTO } from '../models/post/post.dto';
import { UserSessionDTO } from '../models/user/user-session.dto';
import { NotAuthorizedError } from '../common/errors/not-authorized.error';
import { UserRole } from '../common/enums/user-role.enum';
import { PostFullDTO } from '../models/post/post-full.dto';
import { PostStatus } from '../common/enums/post-status.enum';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(PostLike) private readonly postLikesRepository: Repository<PostLike>,

    private readonly mapper: MapperService,
    private readonly photosStorage: PhotoStorageService,
  ) { }

  async findPosts(options: any): Promise<PostDTO[]> {
    const foundPosts: Post[] = await this.postsRepository
      .find({
        where: { isDeleted: false, status: PostStatus.public },
        order: { creationDate: 'DESC' },
        take: options.take || '10',
        skip: options.skip || '0',
      });

    return foundPosts.map((post: Post) => {
      return this.mapper.toPostDTO(post);
    });
  }

  async findPublicPosts(options: any): Promise<PostDTO[]> {
    const foundPosts: Post[] = await this.postsRepository
      .find({
        where: { isDeleted: false, status: 'public' },
        order: { creationDate: 'DESC' },
        take: options.take || '10',
        skip: options.skip || '0',
      });

    return foundPosts.map((post: Post) => {
      return this.mapper.toPostDTO(post);
    });
  }

  async findFollowersPosts(options: any, loggedUserId: number): Promise<any> {
    const foundUser: User = await this.usersRepository.findOne({
      where: { id: loggedUserId, isDeleted: false },
      relations: ['followers'],
    });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    if (foundUser['followers'].length === 0) {
      return [];
    }

    const foundFollowerIds = foundUser['followers'].map((u: User) => u.id);

    const foundPosts = await this.postsRepository.find({
      where: { ['creator']: In(foundFollowerIds), status: 'public', isDeleted: false },
      order: { creationDate: 'DESC' },
      take: options.take || '10',
      skip: options.skip || '0',
    });

    return foundPosts.map((post: PostDTO) => {
      return this.mapper.toPostDTO(post);
    });
  }

  async findFollowingPosts(options: any, loggedUserId: number): Promise<PostDTO[]> {
    const foundUser: User = await this.usersRepository.findOne({
      where: { id: loggedUserId, isDeleted: false },
      relations: ['following'],
    });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    if (foundUser['following'].length === 0) {
      return [];
    }

    const foundFollowingIds = foundUser['following'].map((u: User) => u.id);

    const foundPosts = await this.postsRepository.find({
      where: { ['creator']: In(foundFollowingIds), isDeleted: false },
      order: { creationDate: 'DESC' },
      take: options.take || '10',
      skip: options.skip || '0',
    });

    return foundPosts.map((post: PostDTO) => {
      return this.mapper.toPostDTO(post);
    });
  }

  async findPostById(postId: number, loggedUserId: number): Promise<PostFullDTO> {
    const foundPost: Post = await this.postsRepository
      .findOne({
        where: { isDeleted: false, id: postId },
        relations: ['creator'],
      });

    if (!foundPost) {
      throw new NotFoundPostError();
    }

    const liked: boolean = loggedUserId !== -1
      ? (await this.postLikesRepository.count({
        where: {
          liked: true,
          user: { id: loggedUserId },
          post: { id: postId },
        },
      })) === 1
        ? true
        : false
      : false;

    const likes: number = await this.postLikesRepository.count({
      where: {
        liked: true,
        post: { id: postId },
      },
    });

    return {
      ...this.mapper.toPostFullDTO({
        ...foundPost,
      }),
      creator: this.mapper.toUserDTO(foundPost['__creator__']),
      likes,
      liked,
    };
  }

  async createPost(post: PostCreateDTO, photo: any, userId: number): Promise<PostDTO> {
    const newPost: Post = this.postsRepository.create();

    const foundUser: User = await this.usersRepository
      .findOne({ where: { isDeleted: false, id: userId } });

    if (!foundUser) {
      throw new NotFoundUserError();
    }

    const newPhoto: any = await this.photosStorage.uploadPhoto(photo);

    newPost.title = post.title;
    newPost.description = post.description;
    newPost.status = post.status;
    newPost.creator = Promise.resolve(foundUser);
    newPost.photoLink = newPhoto.photoLink;
    newPost.photoDeleteHash = newPhoto.photoDeleteHash;
    newPost.creationDate = new Date();
    newPost.location = post.location;

    const createdPost: Post = await this.postsRepository.save(newPost);

    return this.mapper.toPostDTO(createdPost);
  }

  async updatePost(postId: number, post: Partial<PostUpdateDTO>, user: UserSessionDTO): Promise<PostFullDTO> {
    const postToUpdate: Post = await this.getValidPost(user, postId);

    await this.postsRepository.update(postId, post);

    return this.mapper.toPostFullDTO({ ...postToUpdate, ...post, creator: postToUpdate['__creator__'] });
  }

  async deletePost(postId: number, user: UserSessionDTO): Promise<PostDTO> {
    const postToDelete: Post = await this.getValidPost(user, postId);

    await this.postsRepository.update(postId, { isDeleted: true, creator: null });

    if (postToDelete.photoDeleteHash) {
      await this.photosStorage.deletePhoto(postToDelete.photoDeleteHash);
    }

    return this.mapper.toPostDTO(postToDelete);
  }

  private async getValidPost(
    sessionUser: UserSessionDTO,
    postId: number,
  ): Promise<Post> {
    const foundPost: Post = await this.postsRepository
      .findOne({ where: { id: postId, isDeleted: false }, relations: ['creator'] });

    if (!foundPost) {
      throw new NotFoundPostError();
    }

    const postCreatorId: number = foundPost['__creator__'].id;

    const canModify: boolean = postCreatorId === sessionUser.id ||
      sessionUser.role === UserRole.Admin;

    if (!canModify) {
      throw new NotAuthorizedError();
    }

    return foundPost;
  }
}
