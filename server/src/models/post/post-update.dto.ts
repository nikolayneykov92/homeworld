import { PostStatus } from './../../common/enums/post-status.enum';
import { IsString, Length, IsEnum, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class PostUpdateDTO {
  @IsString()
  @Length(0, 100)
  @IsOptional()
  @ApiModelProperty({ required: false })
  title: string;

  @IsString()
  @Length(0, 1000)
  @IsOptional()
  @ApiModelProperty({ required: false })
  description: string;

  @IsEnum(PostStatus)
  @IsOptional()
  @ApiModelProperty({ required: false })
  status: PostStatus;
}
