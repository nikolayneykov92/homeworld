import { AuthGuard } from './../core/guards/auth.guard';
import { CreatedPostsResolver } from './../core/resolvers/created-posts.resolver';
import { PostsListComponent } from './posts-list/posts-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FolowersPostsResolver } from '../core/resolvers/folllowers-posts.resolver';
import { FollowingPostsResolver } from '../core/resolvers/following-posts.resolver';

const routes: Routes = [
  {
    path: '',
    component: PostsListComponent
  },
  {
    path: 'people/:userId/posts/created',
    component: PostsListComponent,
    resolve: { data: CreatedPostsResolver },
    canActivate: [AuthGuard]
  },
  {
    path: 'people/:userId/posts/followers',
    component: PostsListComponent,
    resolve: { data: FolowersPostsResolver },
    canActivate: [AuthGuard]
  },
  {
    path: 'people/:userId/posts/following',
    component: PostsListComponent,
    resolve: { data: FollowingPostsResolver },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
  ],
})
export class PostsRoutingModule { }
