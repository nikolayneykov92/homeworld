import { IsString, Length, IsNotEmpty, IsEmail, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserLoginDTO {
  @IsEmail()
  @IsOptional()
  @ApiModelProperty({ example: 'admin@email.com' })
  email?: string;

  @IsString()
  @IsOptional()
  @Length(3, 15)
  @ApiModelProperty({ example: 'admin' })
  username?: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @ApiModelProperty({ example: '123' })
  password: string;
}
