import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { LightboxModule } from 'ngx-lightbox';
import { NgxMasonryModule } from 'ngx-masonry';

@NgModule({
  exports: [
    NgbModule,
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    LightboxModule,
    NgxMasonryModule,
  ],
  entryComponents: [
    ConfirmationComponent,
  ],
  declarations: [ConfirmationComponent]
})
export class SharedModule {
  constructor(@Optional() @SkipSelf() parent: SharedModule) {
    if (parent) {
      return parent;
    }
  }
}
